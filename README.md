
# VectorDB7: Vector Database Operations with Qdrant in Rust

VectorDB7 is a Rust-based project demonstrating how to work with the Qdrant vector database for data ingestion, querying, aggregation, and visualization. This project showcases efficient data handling and retrieval mechanisms within a vector database context using Qdrant, leveraging Rust's performance and safety features.

## Prerequisites

Before you begin, ensure you have the following prerequisites installed on your system:

- **Rust and Cargo**: The Rust programming language and its package manager. Install both from [the official Rust website](https://www.rust-lang.org/tools/install).
- **Docker**: Necessary for running the Qdrant database. Download Docker from [the official Docker website](https://docs.docker.com/get-docker/).

## Setup Guide

### Step 1: Project Setup

#### Create a New Rust Project

Start by initializing a new Rust project using Cargo:

```shell
cargo new vectordb7
cd vectordb7
```

#### Add Dependencies

Add the necessary dependencies to your `Cargo.toml` file:

```shell
cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread
```

These dependencies are essential for your project to communicate with Qdrant and handle asynchronous operations efficiently.

### Step 2: Database Setup

Launch the Qdrant vector database using Docker with the gRPC interface enabled:

```shell
docker run -p 6333:6333 -p 6334:6334 -e QDRANT__SERVICE__GRPC_PORT="6334" qdrant/qdrant
```

This command will start a Qdrant database instance accessible to your Rust application.

### Step 3: Implement Functionality

Implement the logic for data ingestion and querying in the `main.rs` file. This involves creating a new collection in Qdrant, generating and inserting data points, and performing searches to retrieve points based on vector similarity.

### Running the Project

With the Qdrant database running and your Rust application set up, run the following command to start your project:

```shell
cargo run
```

## Deliverables

- **Data Ingestion**: Demonstrates how to generate and insert five unique data points into the Qdrant database, including a 10-dimensional vector and structured payload for each point.
- **Query Functionality**: Shows how to perform a vector search within the Qdrant database, retrieving points based on their similarity to a provided query vector.
- **Visualization**: Provides methods to visualize the query results, either programmatically within Rust or by using the Qdrant web interface for a more interactive examination of the data.

## Visualizations

Visualize the output of your queries by either formatting the output in Rust or by using the Qdrant web interface, which offers an intuitive view of the database's capabilities and the efficiency of your queries.

This project serves as a comprehensive guide for managing high-dimensional vector data using Qdrant and Rust, emphasizing efficient data processing techniques in the context of modern database systems.
