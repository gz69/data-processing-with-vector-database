use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{
    CreateCollection, SearchPoints, VectorParams, VectorsConfig,
};
use serde_json::json;
use rand::Rng;


#[tokio::main]
async fn main() -> Result<()> {
    // Initialize the top-level client to connect to Qdrant on localhost
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Define the name of the collection to use or create
    let collection_name = "test";
    // Delete the collection if it already exists to start fresh
    client.delete_collection(collection_name).await?;

    // Create a new collection named "test" with specific vector configurations
    // Here, we're setting the vector size to 10 and using Cosine distance for comparisons
    client
        .create_collection(&CreateCollection {
            collection_name: collection_name.into(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 10,
                    distance: Distance::Cosine.into(),
                    ..Default::default()
                })),
            }),
            ..Default::default()
        })
        .await?;

    // Insert multiple points with varying data
    let mut rng = rand::thread_rng();
    let points: Vec<PointStruct> = (1..=5).map(|id| {
        // Convert each element of the vector from f64 to f32
        let vector: Vec<f32> = (0..10).map(|_| rng.gen_range(0.0..100.0) as f32).collect();
        let payload: Payload = json!({
            "name": format!("Point {}", id),
            "value": rng.gen_range(1..100),
            "group": format!("Group {}", id % 2),
        }).try_into().unwrap();

        PointStruct::new(id, vector, payload)
    }).collect();

    client.upsert_points_blocking(collection_name, None, points, None).await?;

    // Perform a search without any filters to fetch all points
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![50.; 10], // This is arbitrary; adjust as needed
            filter: None, // No filter to fetch all
            limit: 10,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await?;

    // Simple visualization: Print details of each point
    println!("Found points details:");
    for point in search_result.result {
        println!("ID: {:?}, Payload: {:?}, Score: {}", point.id, point.payload, point.score);
    }

    Ok(())
}
